# README #

1/3スケールのlogocool キーボード MX Keys風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- logicool

## 発売時期

- -

## 参考資料

- logocool キーボード MX Keys(https://www.logicool.co.jp/ja-jp/products/keyboards/mx-keys-wireless-keyboard.920-009299.html?crid=27)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_lgoi_keyboard/raw/4634051429309bf6ab88535b43be968a04cf49a4/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_lgoi_keyboard/raw/4634051429309bf6ab88535b43be968a04cf49a4/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_lgoi_keyboard/raw/4634051429309bf6ab88535b43be968a04cf49a4/ExampleImage.png)
